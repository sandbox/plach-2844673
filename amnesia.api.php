<?php

/**
 * @file
 * Hooks provided by the Amnesia module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Determines whether the specified path should be considered private.
 *
 * @param string $path
 *   The path being checked.
 *
 * @return int
 *   One value among AMNESIA_PAGE_PUBLIC, AMNESIA_PAGE_PRIVATE, and
 *   AMNESIA_PAGE_NEUTRAL:
 *   - if a path is explicitly defined as AMNESIA_PAGE_PRIVATE by any hook
 *     implementation, it will be considered private;
 *   - if a path is explicitly defined as AMNESIA_PAGE_PUBLIC by any hook
 *     implementation and no other implementation defines it as
 *     AMNESIA_PAGE_PRIVATE, it will be considered public;
 *   - if no implementations explicitly defines the path's privacy (i.e.
 *     AMNESIA_PAGE_NEUTRAL is always returned), the path will be considered
 *     private by default.
 */
function hook_amnesia_private_path($path) {
  return $path === 'my-module/my-path' ? AMNESIA_PAGE_PUBLIC : AMNESIA_PAGE_NEUTRAL;
}

/**
 * @} End of "addtogroup hooks".
 */
