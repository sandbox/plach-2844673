(function (window, $, Drupal) {

  Drupal.behaviors.amnesia = {
    attach: function () {
      $(window)
        .on('pageshow', function(event) {
          if (event.originalEvent.persisted) {
            window.document.location.reload();
          }
        })
        .on('beforeunload', function () {
          $('html > *').remove();
        });
    }
  };

})(window, jQuery, Drupal);
